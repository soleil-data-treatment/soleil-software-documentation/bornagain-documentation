# Aide et ressources de BORNAGAIN pour Synchrotron SOLEIL

[<img src="https://www.bornagainproject.org/assets/img/landing-setup.png" width="150"/>](https://www.bornagainproject.org)

## Résumé

- Simulation et fit de spectres GISAXS
- Open source

## Sources

- Code source: https://www.bornagainproject.org/m/
- Documentation officielle: https://jugit.fz-juelich.de/mlz/bornagain

## Navigation rapide

| Tutoriaux | Page pan-data |
| - | - |
[Tutoriel d'installation officiel](lien) | [Documentation pan-data du logiciel](https://software.pan-data.eu/software/165/bornagain) |
| [Tutoriaux officiels](lien) |   |

## Installation

- Systèmes d'exploitation supportés: Windows,  MacOS
- Installation: Facile pour l'interface graphique,  difficile pour le binding python

## Format de données

- en entrée: texte
- en sortie: texte
- sur un disque dur
